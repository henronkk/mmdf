# Multi-modal Data Fusion coursework
# Henri Rönkkö 2018

import numpy as np
import pandas as pd
from sklearn import metrics
from sklearn.svm import SVC
from sklearn import utils

def Threshold(Sample_N, Sample_P):
    scores = np.concatenate((Sample_N, Sample_P), axis=0)
    y = np.concatenate((np.zeros(len(Sample_N)), np.ones(len(Sample_P))), axis=0)
    
    fpr, tpr, thresholds = metrics.roc_curve(y, scores)
    fnr = 1 - tpr
    eer_threshold = thresholds[np.nanargmin(np.absolute((fnr - fpr)))]
    
    return eer_threshold
	
def ComputeERR(Sample_N, Sample_P, thr):
    fa = len(Sample_N[Sample_N>=thr])
    fr = len(Sample_P[Sample_P<thr])
    
    far = fa/len(Sample_N)
    frr = fr/len(Sample_P)
    hter = (far + frr)/ 2
    
    return hter, far, frr

wolves = pd.read_csv('Train_negative.csv')
sheep = pd.read_csv('Train_positive.csv')

wolves_f = wolves['face'].as_matrix()
sheep_f = sheep['face'].as_matrix()

THR = Threshold(wolves_f, sheep_f)
print('Threshold =', THR)

[err, far, frr] = ComputeERR(wolves_f, sheep_f, THR)
print('Training Error =', err*100)

wolves_t = pd.read_csv('Test_negative.csv')
sheep_t = pd.read_csv('Test_positive.csv')
wolves_t_f = wolves_t['face'].as_matrix()
sheep_t_f = sheep_t['face'].as_matrix()

[err_t, far_t, frr_t] = ComputeERR(wolves_t_f, sheep_t_f, THR)
print('Test Error =', err_t*100)

# Task 1: Verification Error from Voice
# Load the training datasets
wolves = pd.read_csv('Train_negative.csv')
sheep = pd.read_csv('Train_positive.csv')

wolves_v = wolves['voice'].as_matrix()
sheep_v = sheep['voice'].as_matrix()

# Compute the decision threshold
THR = Threshold(wolves_v, sheep_v)
print('Threshold =', THR)

# Compute the training error
[err, far, frr] = ComputeERR(wolves_v, sheep_v, THR)
print('Training Error =', err*100)

# Load the testing datasets
wolves_t = pd.read_csv('Test_negative.csv')
sheep_t = pd.read_csv('Test_positive.csv')

wolves_t_v = wolves_t['voice'].as_matrix()
sheep_t_v = sheep_t['voice'].as_matrix()

# Compute the testing error
[err_t, far_t, frr_t] = ComputeERR(wolves_t_v, sheep_t_v, THR)
print('Testing Error =', err_t*100)

# Task 2: Verification Error from Face and Voice using a Simple Sum
# Load the training datasets
wolves = pd.read_csv('Train_negative.csv')
sheep = pd.read_csv('Train_positive.csv')

wolves_f = wolves['face'].as_matrix()
sheep_f = sheep['face'].as_matrix()
wolves_v = wolves['voice'].as_matrix()
sheep_v = sheep['voice'].as_matrix()

# Scores for the face and voice are summed together without weighting
scores_f = np.concatenate((wolves_f, sheep_f), axis=0)
scores_v = np.concatenate((wolves_v, sheep_v), axis=0)
scores = scores_f + scores_v
y = np.concatenate((np.zeros(len(wolves_f)), np.ones(len(sheep_f))), axis=0)

# Compute the decision threshold
fpr, tpr, thresholds = metrics.roc_curve(y, scores)
fnr = 1 - tpr
eer_threshold = thresholds[np.nanargmin(np.absolute((fnr - fpr)))]

print('Threshold =', eer_threshold)

# Compute the training error
[err, far, frr] = ComputeERR((wolves_f + wolves_v) , (sheep_f + sheep_v), eer_threshold)
print('Training Error =', err*100)

# Load the testing datasets
wolves_t = pd.read_csv('Test_negative.csv')
sheep_t = pd.read_csv('Test_positive.csv')

wolves_t_f = wolves_t['face'].as_matrix()
sheep_t_f = sheep_t['face'].as_matrix()
wolves_t_v = wolves_t['voice'].as_matrix()
sheep_t_v = sheep_t['voice'].as_matrix()

wolves_t_sum = wolves_t_f + wolves_t_v
sheep_t_sum = sheep_t_f + sheep_t_v

# Compute the testing error
[err_t, far_t, frr_t] = ComputeERR(wolves_t_sum, sheep_t_sum, eer_threshold)
print('Testing Error =', err_t*100)

# Task 3: Weighted Sum Fusion.
# Load the training datasets
wolves = pd.read_csv('Train_negative.csv')
sheep = pd.read_csv('Train_positive.csv')

wolves_f = wolves['face'].as_matrix()
sheep_f = sheep['face'].as_matrix()
wolves_v = wolves['voice'].as_matrix()
sheep_v = sheep['voice'].as_matrix()

THR_F = Threshold(wolves_f, sheep_f)
print('Face Threshold =', THR_F)
THR_V = Threshold(wolves_v, sheep_v)
print('Voice Threshold =', THR_V)

# Compute errors for the face and voice separately
[err_f, far_f, frr_f] = ComputeERR(wolves_f, sheep_f, THR_F)
print('Face Training Error =', err_f*100)
[err_v, far_v, frr_v] = ComputeERR(wolves_v, sheep_v, THR_V)
print('Voice Training Error =', err_v*100)

# Normalized and inversed weights for the errors (small error gets bigger weight and vice versa)
weight_face = 1/(1+err_f/err_v)
weight_voice = 1/(1+err_v/err_f)

print('Face weight =', weight_face)
print('Voice weight =', weight_voice)

# Weighted scores are summed together
scores_f = np.concatenate((wolves_f, sheep_f), axis=0)
scores_v = np.concatenate((wolves_v, sheep_v), axis=0)
scores = weight_face*scores_f + weight_voice*scores_v

# Compute the training threshold again
y = np.concatenate((np.zeros(len(wolves_f)), np.ones(len(sheep_f))), axis=0)
fpr, tpr, thresholds = metrics.roc_curve(y, scores)
fnr = 1 - tpr
eer_threshold = thresholds[np.nanargmin(np.absolute((fnr - fpr)))]

print('Weighted threshold =', eer_threshold)

# Compute the training error
[err, far, frr] = ComputeERR((weight_face*wolves_f + weight_voice*wolves_v) , (weight_face*sheep_f + weight_voice*sheep_v), eer_threshold)
print('Weighted Training Error =', err*100)

# Load the testing set
wolves_t = pd.read_csv('Test_negative.csv')
sheep_t = pd.read_csv('Test_positive.csv')

wolves_t_f = wolves_t['face'].as_matrix()
sheep_t_f = sheep_t['face'].as_matrix()
wolves_t_v = wolves_t['voice'].as_matrix()
sheep_t_v = sheep_t['voice'].as_matrix()

# Testing error is now calculated with the new threshold 
[err_t, far_t, frr_t] = ComputeERR(wolves_t_f + wolves_t_v, sheep_t_f + sheep_t_v, eer_threshold)
print('Testing Error =', err_t*100)

# Task 4: Implement One Classifier-based Fusion (SVM or Logistic Regression)
# Load the training datasets
wolves = pd.read_csv('Train_negative.csv')
sheep = pd.read_csv('Train_positive.csv')

wolves_f = wolves['face'].as_matrix()
sheep_f = sheep['face'].as_matrix()
wolves_v = wolves['voice'].as_matrix()
sheep_v = sheep['voice'].as_matrix()

scores_f = np.concatenate((wolves_f, sheep_f), axis=0)
scores_v = np.concatenate((wolves_v, sheep_v), axis=0)

scores = np.stack((scores_f, scores_v), axis=1)

y = np.concatenate((np.zeros(len(wolves_f)), np.ones(len(sheep_f))), axis=0)

# Load the testing datasets
wolves_t = pd.read_csv('Test_negative.csv')
sheep_t = pd.read_csv('Test_positive.csv')

wolves_t_f = wolves_t['face'].as_matrix()
sheep_t_f = sheep_t['face'].as_matrix()
wolves_t_v = wolves_t['voice'].as_matrix()
sheep_t_v = sheep_t['voice'].as_matrix()

# Scale test set size to match the training set size with random sampling
wolves_t_f = utils.resample(wolves_t_f, n_samples=len(wolves_f))
wolves_t_v = utils.resample(wolves_t_v, n_samples=len(wolves_v))
sheep_t_f = utils.resample(sheep_t_f, n_samples=len(sheep_f))
sheep_t_v = utils.resample(sheep_t_v, n_samples=len(sheep_v))

scores_t_f = np.concatenate((wolves_t_f, sheep_t_f), axis=0)
scores_t_v = np.concatenate((wolves_t_v, sheep_t_v), axis=0)

scores_t = np.stack((scores_t_f, scores_t_v), axis=1)
y_t = np.concatenate((np.zeros(len(wolves_t_f)), np.ones(len(sheep_t_f))), axis=0)

# SVM Classifier
svclassifier = SVC(kernel='linear')  
svclassifier.fit(scores, y_t)
predicted = svclassifier.predict(scores)
precision = metrics.precision_score(predicted, y_t)

print('Accuracy: {0:0.4f}'.format(precision*100))
print('Error: {0:0.4f}'.format((1-precision)*100))